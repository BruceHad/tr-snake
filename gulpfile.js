const { series, src, dest, watch } = require('gulp');
const sass = require('gulp-sass');
const fs = require('fs');
const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');
const path = require('path');
const data = require('gulp-data');
const del = require('del');

function clean(cb) {
  del(['./public/*']);
  cb();
}

function css(cb) {
  src('src/css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./public/css'));
  cb();
}

function html(cb) {
  let date = new Date();
  let templateData = {
    lastUpdated: date.toDateString()
  };

  let options = {
    ignorePartials: true,
    batch: ['./src/templates/partials']
  };

  src('src/templates/*.hbs')
    .pipe(data(function(file) {
      // get data from json
      return JSON.parse(fs.readFileSync('./src/data/tr-data.json'));
    }))
    .pipe(handlebars(templateData, options))
    .pipe(rename(function(path) {
      path.extname = '.html';
    }))
    .pipe(dest('./public'));
  cb();
}

function js(cb) {
  src('src/**/*.js')
    .pipe(dest('./public'));
  cb();
}

function images(cb) {
  src('src/images/*.*')
    .pipe(dest('./public/images'));
  cb();
}

let updateEverything = series(clean, css, html, js, images);

let watcher = watch('./src/**/*.*');

watcher.on('all', function(event, path, stats) {
  console.log('File ' + path + ' was ' + event + ', running tasks...');
  updateEverything();
});

exports.default = updateEverything;